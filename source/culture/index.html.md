---
layout: markdown_page
title: "GitLab Culture"
---

## Introduction

Please see our [primer](https://about.gitlab.com/primer/) for more general information about GitLab.

## Advantages

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't _feel_ remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focussed on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

## Historical Anecdotes

### _October 8th, 2011_

Dmitriy started GitLab when he pushed the [initial commit](https://gitlab.com/gitlab-org/gitlab-ce/commit/9ba1224867665844b117fa037e1465bb706b3685).

### _August 24th, 2012_

Sid announced [GitLab on HN](https://news.ycombinator.com/item?id=4428278).

### _September 14th, 2012_

[First 10 people get access](https://about.gitlab.com/2012/09/14/first-10-people-got-access/)
to GitLab Cloud (now known as GitLab.com).

### _November 13th, 2012_

[GitLab CI is officially announced](https://about.gitlab.com/2012/11/13/continuous-integration-server-from-gitlab/).

### _July 22nd, 2013_

[GitLab Enterprise Edition is announced](https://about.gitlab.com/2013/07/22/announcing-gitlab-enterprise-edition/).

### _April 18th, 2014_

[GitLab Cloud renamed to GitLab.com](https://about.gitlab.com/2014/04/18/gitlab-cloud-becomes-gitlab-com/).

### _March 4th, 2015_

[GitLab in Y Combinator winter 2015 batch](https://about.gitlab.com/2015/03/04/gitlab-is-part-of-the-y-combinator-family/).

### _August 15th, 2015_

Series A Funding was signed.

### _October 10th, 2015_

Anniversary of our first ever summit in Amsterdam with 25 team members.

## Summits

Approximately every 6-9 months, we gather in person to see how tall everyone is (hard to see with video conferencing), and to
get to know each other better by working and 'playing' together. Here are some
 [impressions from our first Summit in Amsterdam, October 2015](https://about.gitlab.com/2015/11/30/gitlab-summit-2015/).


## Team Stories

What better way to convey a sense of who we are and how we work together, than by sharing the stories about it?

### The Boat

<!-- HTML blocks below - applied to make the images and the video more harmonic than 1 single column with each on a different "row" -->

<br>

<div class="row">
  <div class="col-sm-8 col-xm-12"><img src="../images/team/boat.jpg" alt="The Boat"></div>
  <div class="col-md-4 col-xm-12">
    <p class="justify"><a href="https://about.gitlab.com/2016/01/06/our-y-combinator-experience/">Back then</a>, the whole team used to fit in one car. And the car was called "the Boat".</p>
  </div>
</div>

<br>

<div class="row">
  <div class="col-sm-8 col-xm-12">
    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/4TnKmrpiSgQ" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>
  </div>
  <div class="col-sm-4 col-xm-12">
    <p class="justify">We even took the Boat from San Francisco to Las Vegas to celebrate Job's bachelor party, but as you can see in this video, he thought we were going to visit a customer in Los Angeles!</p>
  </div>
</div>

<br>

### The cattle

<br>

<div class="row">
  <div class="col-md-4 col-xm-12"><img class="cattle" src="../images/cattle_stare.JPG" alt="Staring down cattle"></div>
  <div class="col-md-8 col-xm-12">
    <h4 class="media-heading">Staring down the cattle?</h4>
    <p class="justify-1">Our CFO, Paul, was on vacation on a cattle ranch,
    during a time of fundraising. Normally vacation is vacation of course, but
    in this case it was necessary to have some calls now and again which
    required strong internet. To get to strong internet, Paul had to cross
    fields with cattle in them, and stare them down. Over the course of many
    trips he learned that cattle are docile, mostly... but don't turn your back
    on them because they can't be outrun!</p>
  </div>
</div>

## Internal Feedback

By sending out our Feedback Form (find the link to the "open" form by accessing
[this "closed" document](https://docs.google.com/document/d/12ZAACGeS2_nz6AFXqf78x9iv2LB_hyX9fcT4s9nJF4M/edit))
we gather feedback from the team members anonymously. We then share the main highlights
and concerns / wishes / things people wonder about with the entire team by discussing
them during our [Team Call](https://about.gitlab.com/handbook/#team-call) and posting
all topics that came up along with their responses here (with the exception of
[topics](https://about.gitlab.com/handbook/#general-guidelines) that by nature
are not shared outside of the company). _The responses may be altered from the original wording in an effort to maintain anonymity, while also maintaining the same spirit and message of the response._

### Feedback from January 2016, and responses

#### "What do you wish we had / What are you wondering about"

1. "Contractor or employee? Worried about job security as a contractor."
   * We value all team members equally, regardless of the legal arrangement that
   you have with GitLab. Due to legal restrictions and the difficulty of having
   people be employees outside of the US or NL (where we have a legal entity), a
   large portion of the team are contractors (21 out of 46). At GitLab, as everywhere
   else, job security relies mostly on how you are performing as a team member,
   and how the company performs as a whole. By the way, the contracts that we use
   are all viewable on https://about.gitlab.com/handbook/contracts/
1. "More patience and consideration with ideas from newer people, things are sometimes
quickly rejected as 'won't work' or 'not interested' without much explanation."
   * It is difficult to comment on what may have been specific circumstances,
   but if you have felt that your idea was rejected too quickly or without explanation,
   then please know that this was not intended to be unkind or harsh. There are
   many ideas and for the sake of efficiency we give minimal reasoning in responding.
   This is also due to the nature of asynchronous communication where it is hard
   to tell if an answer is extensive enough to satisfy the question. If you feel
   that an idea is being rejected rather quickly, you **can** and **should**
   request more explanation. This might lead to a fruitful discussion and a reconsideration.
 1. "Wish we had more time"/ "Wonder if sometimes we go too fast and should go slower
 to focus on quality and testing more"
    * Please make sure you take enough
    [time off](https://about.gitlab.com/handbook/#paid-time-off) to recharge!
    Having a rapid release cycle contributes to increasing quality over time being able to iterate faster. For
    particular concerns in an individual issue, please raise your concerns in the
    issue. Because GitLab has gotten very popular the absolute amount of bugs might be increasing.
    But to individual users it feels like we've "[been squashing bugs and releasing
    features rapidly while also decreasing the number of regressions introduced and
    improving their test suit across the board](https://news.ycombinator.com/item?id=11039966)".
    Obviously quality is and will always be a top concern, especially code quality
    since that enables better testing, faster fixes, and more contributions.
1. "Better supervision in the first two months"
   * Always be sure to reach out to your manager if you feel that you need more guidance. Your peers are a Slack message away.
1. "Maybe a more competitive salary"
   * From the [handbook](https://about.gitlab.com/handbook/#general-guidelines): "If you are unhappy with anything (your duties, your colleague, your boss, your salary, your location, your computer) please let your boss, or the CEO, know as soon as you realize it. We want to solve problems while they are small."
1. "A step between review/QA and deployment."
   * Review happens through merge requests, and QA happens as part of the release process. We can't think of anything else at the moment that would not introduce gates that cause delay and inefficiency.
1. "Off-site meetings during the year and/or team-specific summits, like a hackathon"
   * We had a Summit in Oct'15, and are having one in May'16. We aim to have these every 6-9 months. We are thinking about organizing a hackathon during the [Summit](https://gitlab.com/summits/Austin-Summit-2016-project/blob/master/Proposed_Activities.md); please upvote if you want it to happen!
1. "We share a lot about our software 'side'; can we share more about our other 'sides' (like how marketing does marketing) in a "team blog"? This would help in hiring awesome people!"
   * We love blog posts and sharing all of GitLab's 'sides'. Please channel your inner need to write a blog post (or part of it!) on our [blog](https://gitlab.com/gitlab-com/blog-posts/issues) repo.
1. "Additional training for dev people, maybe? Specific suggestions for topics include agile coding, code quality, and there is a suggestion for Robert "Uncle Bob " Marcin (https://sites.google.com/site/unclebobconsultingllc/) for code quality
  * After some research within the team there was a greater interest in a "birds of a feather" gathering than formal training. We're looking to do this during our Summit trip to Austin.
1. "What features are we planning? I hear about price changes and optional features?"
  * See [issue about pricing](https://dev.gitlab.org/gitlab/organization/issues/522) and see /direction and GitLab Option List Doc for optional features.
1. "How are we doing? How does the Board see it? Can we keep up the growth and the Sales?"
  * Khosla is really happy so far. Keeping up growth depends on demand generation
1. "If we are sometimes too open and too transparent in a way that hurts us?"
  * So far the only negative thing is that we suspect that competitors release their stuff early to preempt us, we think it is great that they are adjusting their schedule to ours!
1. "Creating/using a standard for interviewing candidates."
  * Good idea, a lot of us use standard interview questions https://about.gitlab.com/handbook/hiring/#interview-questions but merge requests are welcome.
1.  "More formality in development process."
  * What form of formality? We welcome changes that don't reduce our productivity. Potentially we could start using issue weights more to gauge the load.

#### "What do you like about working here?"

1. A lot of love for the team
   * Keywords people used to describe their fellow team members are: 'talented,
   caring, teamwork, approachable, honest, frank, smart, brilliant, skilled,
   team spirit'.
1. Great product
   * Four people mentioned it specifically, and this one sums it up nicely: "Working
   on a product that I actually love to use". Respondents also mentioned the fast pace of development.
1. Open Source
   * Respondents value being involved in Open Source, with phrases such as: 'proving that open source is awesome and working with the rest of the community, working on open source while getting paid for it is a dream job!'
1. Freedom and opportunity
   * We are a remote-first company and our team members like: 'being remote-first, working from home, having personal independence, the freedom to choose what to work on, the freedom that you don't get in a corporate office environment, flexibility to get things done, slim process, slack oriented'
   * People also mentioned the opportunity to learn, and the massive opportunity that the project and the company has.
1. Team dynamic
   * Several people mentioned that they really like the team dynamic, specifically: 'the support that you need is there, you have the ability to take on multiple hats and responsibilities, everyone and everything is open to constant improvement, ability to collaborate even though we're remote, we're remote but still have a great sense of "team", love the cross discipline collaboration that goes on every day'

### Feedback from the GitLab team - September 2015

Summarized in this presentation about ["Stuff the GitLab team likes and does not
like"](https://docs.google.com/a/gitlab.com/presentation/d/1h9P8Vf_6fzPbLCCahvwtIF5j_cH54zsv9iRSseVZzl0/edit?usp=sharing),
here is what the team said in September of 2015.

#### What we love about working at GitLab

Freedom, flexibility, enthusiasm, passion, great, smart, engaging,
enjoy teaching, feels like a family, great dynamic, supportive,
approachable execs, supportive, transparency, speed of innovation, remote working,
company sponsored training, great Summit in Amsterdam.

_and also_

Great product, ability to create new processes,
company growth, no burocracy, no high pressure, opportunity, our growth,
the challenge of maintaining quality of people, product, brand etc,
laser focus on improving collaboration through social coding, market adoption,
our work is public so we can talk about it, and our ability to create new processes.

#### What we wish we had or what we want to be doing & What we’re doing about it

1. More team members.
   * We’re hiring
1. Bigger feature gap between CE and EE.
   * Current plan is to have one EE feature added per release, so over time the difference will grow.
1. More summits.
   * Current plan is to have a summit every 6-9 months.
1. Better onboarding.
   * We're working on this with GitLabUniversity (GLU) content, continuous improvements to documentation and the handbook
1. Customer success events, starting in the Bay Area.
   * Cool idea, start an issue and make it happen!
1. Coaching on Agile and Lean approaches for Engineering team.
   * Great idea, we'll look into it. Suggestions are welcome.
1. A scale with happiness of last work week for feedback.
   * We've working this into our feedback from in a different setting for now.
1. Global presence of Service Engineers, and dedicated trainer and training materials.
   * We're working on this through hiring. Also, we have high hopes of GLU, ongoing content creation, etc. to help out here.

#### What we’re wondering about & the answers to our thoughts

1. Stock options: terms, conditions
   * Does the handbook answer your questions? (https://about.gitlab.com/handbook/stock-options). Please feel free to ask Paul.
1. What the company will look like 3, 6, 12 months from now? (community-ish, enterprise-ish)
   * The community and people using GitLab will keep growing. 12 months from now
  we'll answer all questions from the community on all platforms (from forum to
  Stack Overflow), we
  'll have a proper swag shop and have more developer oriented
  content (blog posts, video's).
   * Enterprise wise we'll have double the features we have now, a twice as large sales
team, and many add-ons. Feel free to ask something more specific if you need more
detail. And of course try to shape it as you think it should look.
1. How committed are we to building EE features and having significant releases  each month?
   * Very committed. At least one tentpole EE feature every month. Next 3 releases contain 2 or 3 each! https://about.gitlab.com/direction/
1. Sales team hiring plan
   * We're hiring but it is not a priority until everyone is up to speed and trained. But we expect that to happen soon, and the marketing machine will come up to speed soon, after which hiring becomes a priority.

<!-- particular styles for the pictures and video at the beginning -->

<style>
.justify,.justify-1 { text-align: justify; }
.media-heading { margin-bottom: 5px; }

@media (max-width: 767px) {
.col-sm-8 h4,.justify { padding-top: 10px; }
}
@media (max-width: 991px) {
.col-md-8 h4,.justify-1 { padding-top: 10px; }
}
</style>
